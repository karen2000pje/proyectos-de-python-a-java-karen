
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Usuario
 */
//Desarrolle un algoritmo que permita calcular el promedio de notas de los exámenes de un estudiante.
//El algoritmo solo permite ingresar una nota a la vez.
//Considere que se realizan únicamente tres exámenes (nota1, nota2, nota3).
public class Promedionotas {

    public static void promnot() {
        Scanner note = new Scanner(System.in);
        int i = 1; // el contador que permite iterar hasta que se cumpla la condición
        double promedio = 0; //variable que permite almacenar la sumatoria de las notas
        while (i <= 3) { // condición que solo permita en ingreso de 3 notas
            int nota;
            System.out.println("Digite la nota: ");// solicitamos al usuario que ingrese la nota
            nota = note.nextInt();
            promedio = promedio + nota;// se va acumulando la sumatoria de notas
            i++; //ir incrementando de uno en uno el contador
            promedio = promedio / 3;// una vez que se tiene la suma de todas las notas se dividen entre 3
            System.out.println("El promedio es: " + promedio);
            
        }
    }

}
