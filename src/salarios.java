
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Usuario
 */
public class salarios {
    //Se requiere un algoritmo para calcular la planilla de una persona
//Salario bruto, salario neto, teniendo en consideración que al empleado se le deduce el 9.17% de su salario como cargas sociales.
// Para ello se dispone de las horas laboradas por mes y el monto por hora.
//Debe mostrar el salario bruto, el salario neto y el monto de las deducciones.

    public static void salariosprogra() {
        Scanner sal = new Scanner(System.in);
        int horaslaboradas;
        int preciohora;
        double salarioBruto;
        double salarioNeto;
        double deducciones;
        System.out.println("Digite la cantidada de horas laboradas en el mes: ");
        horaslaboradas = sal.nextInt();
        System.out.println("Digite el precio por hora: ");
        preciohora = sal.nextInt();
        salarioBruto = horaslaboradas * preciohora;
        salarioNeto = salarioBruto - salarioBruto * (0.0917 / 100);
        deducciones = salarioBruto - salarioNeto;
        System.out.println("El salario bruto es: " + salarioBruto);
        System.out.println("El salario neto es: " + salarioNeto);
        System.out.println("El total de deducciones es: " + deducciones);

    }
}



